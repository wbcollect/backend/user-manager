# Use a base image with Gradle and JDK for building the application
FROM gradle:7.2.0-jdk17 AS builder

ARG BUILD_COMMON_REPO_TYPE
ARG BUILD_COMMON_REPO_TOKEN

ENV BUILD_COMMON_REPO_TYPE=$BUILD_COMMON_REPO_TYPE
ENV BUILD_COMMON_REPO_TOKEN=$BUILD_COMMON_REPO_TOKEN

# Set the working directory inside the container
WORKDIR /app

# Copy the Gradle build files to the container
COPY build.gradle .
COPY settings.gradle .
COPY gradlew .

# Make gradlew executable
RUN chmod +x gradlew

# Copy the Gradle wrapper files to the container
COPY gradle gradle

# Copy the application source code to the container
COPY src src

# Build the application using Gradle without tests
RUN ./gradlew build -x test

# Use a smaller JRE base image for running the application
FROM eclipse-temurin:17-jre-focal

# Set the working directory inside the container
WORKDIR /app

# Copy the built JAR file from the builder stage
COPY --from=builder /app/build/libs/user-manager-0.0.1-SNAPSHOT.jar .

# Expose the gRPC server port
EXPOSE 9090

# Set the command to run the application when the container starts
CMD ["java", "-jar", "user-manager-0.0.1-SNAPSHOT.jar"]
