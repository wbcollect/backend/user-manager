# Микросервис User Manager

## Описание

Микросервис User Manager отвечает за создание, изменение и получение информации о пользователях.

## Технологии

- Spring Boot + Gradle
- Spring Data JPA
- gRPC
- Postgres + Flyway
