package com.wbcollect.usermanager.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "users")
public class User {
    @Id
    @Column
    Long id;

    @Column
    String username;

    @Column
    String password;

    @Column
    String email;

    @Column
    String status;
}
