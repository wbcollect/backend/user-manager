package com.wbcollect.usermanager.util;

import com.wbcollect.usermanager.UserManagerProto;
import com.wbcollect.usermanager.entity.User;
import lombok.experimental.UtilityClass;

@UtilityClass
public class Utils {

    public static UserManagerProto.User mapUser(User entity) {
        return UserManagerProto.User.newBuilder()
                .setId(entity.getId())
                .setUsername(entity.getUsername())
                .setPassword(entity.getPassword())
                .setEmail(entity.getEmail())
                .setStatus(entity.getStatus())
                .build();
    }

}
