package com.wbcollect.usermanager.grpc;

import com.wbcollect.usermanager.UserManagerGrpc;
import com.wbcollect.usermanager.UserManagerProto;
import com.wbcollect.usermanager.entity.User;
import com.wbcollect.usermanager.repository.UserRepository;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.service.GrpcService;

import static com.wbcollect.usermanager.util.Utils.mapUser;

@Slf4j
@GrpcService
@RequiredArgsConstructor
public class UserService extends UserManagerGrpc.UserManagerImplBase {
    private final UserRepository userRepository;

    @Override
    public void getUser(UserManagerProto.UserRequest request, StreamObserver<UserManagerProto.UserReply> responseObserver) {
        log.info("Received request to get info about user with email: '" + request.getEmail() + "'");
        try {
            var entity = userRepository.findByEmail(request.getEmail());
            if (entity.isEmpty()) {
                var errorMessage = "User with email: '" + request.getEmail() + "' not found";
                log.warn(errorMessage);
                responseObserver.onError(Status.NOT_FOUND.withDescription(errorMessage).asRuntimeException());
                return;
            }
            responseObserver.onNext(UserManagerProto.UserReply.newBuilder().setUser(mapUser(entity.get())).build());
            responseObserver.onCompleted();
            log.info("Returned info for user with email: '" + request.getEmail() + "'");
        } catch (Throwable t) {
            log.error("Error occurred when getting info about user with id: " + request.getId() + ", email: '" + request.getEmail() + "'", t);
            responseObserver.onError(Status.INTERNAL.withDescription("Internal server error").withCause(t).asRuntimeException());
        }
    }

    @Override
    public void createUser(UserManagerProto.CreateUserRequest request, StreamObserver<UserManagerProto.CreateUserReply> responseObserver) {
        log.info("Received request to create user with email: '" + request.getEmail() + "'");
        try {
            if (userRepository.existsByEmail(request.getEmail())) {
                var errorMessage = "User with email: '" + request.getEmail() + "' already exists";
                log.warn(errorMessage);
                responseObserver.onError(Status.ALREADY_EXISTS.withDescription(errorMessage).asRuntimeException());
                return;
            }
            User entity = new User(
                    System.currentTimeMillis(),
                    request.getUsername(),
                    request.getPassword(),
                    request.getEmail(),
                    "NEW"
            );
            userRepository.save(entity);
            var user = mapUser(entity);
            responseObserver.onNext(UserManagerProto.CreateUserReply.newBuilder().setUser(user).build());
            responseObserver.onCompleted();
            log.info("User with email: '" + request.getEmail() + "' created successfully");
        } catch (Throwable t) {
            log.error("Error occurred when creating user with email: '" + request.getEmail() + "'", t);
            responseObserver.onError(Status.INTERNAL.withDescription("Internal server error").withCause(t).asRuntimeException());
        }
    }

    @Override
    public void changeUser(UserManagerProto.ChangeUserRequest request, StreamObserver<UserManagerProto.ChangeUserReply> responseObserver) {
        log.info("Received request to change user with email: '" + request.getEmail() + "'");
        try {
            var entity = userRepository.findByEmail(request.getEmail());
            if (entity.isEmpty()) {
                var errorMessage = "User with email: '" + request.getEmail() + "' not found";
                log.warn(errorMessage);
                responseObserver.onError(Status.NOT_FOUND.withDescription(errorMessage).asRuntimeException());
                return;
            }
            var user = entity.get();
            var newUsername = request.getUsername();
            var newPassword = request.getPassword();
            var newStatus = request.getStatus();
            if (!"".equals(newUsername))
                user.setUsername(newUsername);
            if (!"".equals(newPassword))
                user.setPassword(newPassword);
            if (!"".equals(newStatus))
                user.setStatus(newStatus);
            userRepository.save(user);
            responseObserver.onNext(UserManagerProto.ChangeUserReply.newBuilder().setUser(mapUser(user)).build());
            responseObserver.onCompleted();
            log.info("User with email: '" + request.getEmail() + "' changed successfully");
        } catch (Throwable t) {
            log.error("Error occurred when changing user with email: '" + request.getEmail() + "'", t);
            responseObserver.onError(Status.INTERNAL.withDescription("Internal server error").withCause(t).asRuntimeException());
        }
    }
}
