alter table users add salt varchar(100);
alter table users add hash varchar(100);
update users set salt = gen_random_uuid();
update users set hash = encode(sha256((salt || password)::bytea), 'base64');
