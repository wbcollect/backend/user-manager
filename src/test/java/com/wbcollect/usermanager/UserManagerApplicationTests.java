package com.wbcollect.usermanager;

import com.wbcollect.initializers.PostgresInitializer;
import com.wbcollect.usermanager.entity.User;
import com.wbcollect.usermanager.grpc.UserService;
import io.grpc.internal.testing.StreamRecorder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

@Testcontainers
@DirtiesContext
@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(initializers = {
        PostgresInitializer.class,
})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class UserManagerApplicationTests {

    @Container
    private static final PostgreSQLContainer POSTGRE_SQL_CONTAINER = PostgresInitializer.POSTGRE_SQL_CONTAINER;

    @Autowired
    private UserService userService;

    private static Stream<User> provideUsers() {
        return Stream.of(new User(System.currentTimeMillis(), "Ivan", "password", "ivan42@gmail.com", "NEW"),
                new User(System.currentTimeMillis(), "Oleg", "Password", "oleg1@mail.ru", "NEW"),
                new User(System.currentTimeMillis(), "John", "pass1234", "just_john@outlook.com", "NEW"),
                new User(System.currentTimeMillis(), "Mike", "amazing_pw1", "cool_mike@gmail.com", "NEW"));
    }

    @Transactional
    @ParameterizedTest
    @MethodSource("provideUsers")
    public void createAndGetUser(User testUser) throws Exception {
        String username = testUser.getUsername(),
                password = testUser.getPassword(),
                email = testUser.getEmail();
        var request = UserManagerProto.CreateUserRequest.newBuilder()
                .setUsername(username)
                .setPassword(password)
                .setEmail(email)
                .build();
        StreamRecorder<UserManagerProto.CreateUserReply> createUserReplyResponseObserver = StreamRecorder.create();
        // sending request to create user
        userService.createUser(request, createUserReplyResponseObserver);
        if (!createUserReplyResponseObserver.awaitCompletion(5, TimeUnit.SECONDS)) {
            fail("The call did not terminate in time");
        }
        assertNull(createUserReplyResponseObserver.getError());
        var createUserReply = createUserReplyResponseObserver.getValues();
        var userEmail = createUserReply.get(0).getUser().getEmail();

        var userRequest = UserManagerProto.UserRequest.newBuilder().setEmail(userEmail).build();
        StreamRecorder<UserManagerProto.UserReply> getUserReplyResponseObserver = StreamRecorder.create();
        // sending request to get user
        userService.getUser(userRequest, getUserReplyResponseObserver);
        if (!getUserReplyResponseObserver.awaitCompletion(5, TimeUnit.SECONDS)) {
            fail("The call did not terminate in time");
        }
        assertNull(getUserReplyResponseObserver.getError());
        var getUserReply = getUserReplyResponseObserver.getValues().get(0);
        var user = getUserReply.getUser();
        // check that sent info matches with gotten
        assertThat(user.getId()).isGreaterThanOrEqualTo(0L);
        assertEquals(username, user.getUsername());
        assertEquals(password, user.getPassword());
        assertEquals(email, user.getEmail());
        assertEquals("NEW", user.getStatus());
    }

    @Test
    @Transactional
    public void createUserAndChangeUsernameAndPassword() throws Exception {
        String username = "Ivan",
                password = "bad_password",
                email = "ivan42@gmail.com";
        var createUserRequest = UserManagerProto.CreateUserRequest.newBuilder()
                .setUsername(username)
                .setPassword(password)
                .setEmail(email)
                .build();
        StreamRecorder<UserManagerProto.CreateUserReply> createUserReplyResponseObserver = StreamRecorder.create();
        // sending request to create user
        userService.createUser(createUserRequest, createUserReplyResponseObserver);
        if (!createUserReplyResponseObserver.awaitCompletion(5, TimeUnit.SECONDS)) {
            fail("The call did not terminate in time");
        }
        assertNull(createUserReplyResponseObserver.getError());
        var userReply = createUserReplyResponseObserver.getValues().get(0).getUser();
        assertThat(userReply.getId()).isGreaterThanOrEqualTo(0L);
        assertEquals(username, userReply.getUsername());
        assertEquals(password, userReply.getPassword());
        assertEquals(email, userReply.getEmail());
        assertEquals("NEW", userReply.getStatus());

        String newUsername = "Ivan Ivanov",
                newPassword = "good_password",
                newStatus = "SUBSCRIPTION_PAID";

        StreamRecorder<UserManagerProto.ChangeUserReply> changeUserReplyStreamRecorder = StreamRecorder.create();
        var changeUserRequest = UserManagerProto.ChangeUserRequest.newBuilder()
                .setUsername(newUsername)
                .setPassword(newPassword)
                .setEmail(email)
                .setStatus(newStatus)
                .build();
        // sending request to create user
        userService.changeUser(changeUserRequest, changeUserReplyStreamRecorder);
        if (!createUserReplyResponseObserver.awaitCompletion(5, TimeUnit.SECONDS)) {
            fail("The call did not terminate in time");
        }
        assertNull(createUserReplyResponseObserver.getError());
        userReply = changeUserReplyStreamRecorder.getValues().get(0).getUser();
        assertThat(userReply.getId()).isGreaterThanOrEqualTo(0L);
        assertEquals(newUsername, userReply.getUsername());
        assertEquals(newPassword, userReply.getPassword());
        assertEquals(email, userReply.getEmail());
        assertEquals(newStatus, userReply.getStatus());
    }
}